const mongoose = require('mongoose');
const moment = require('moment');

const Schema = mongoose.Schema;
mongoose.Promise = Promise;

const Extraction = new Schema({
  numbers: { type: Array },
  numbersHash: { type: String },
  date: {
    type: Date,

    default: () => {
      return moment().toDate();
    }
  }
});

module.exports = mongoose.model("Extraction", Extraction);