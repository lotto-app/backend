const mongoose = require('mongoose');
const moment = require('moment');

const Schema = mongoose.Schema;
mongoose.Promise = Promise;

const Ticket = new Schema({
  numbers: { type: Array },
  numbersHash: { type: String },
  date: {
    type: Date,

    default: () => {
      return moment().toDate();
    }
  }
});

Ticket.pre('save', function(next) {
  this.numbersHash = this.numbers.join('');
  next();
});

module.exports = mongoose.model("Ticket", Ticket);