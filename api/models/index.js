const mongoose = require('mongoose');
const config = require('../../config');

const ExtractionModel = require('./extraction');
const TicketModel = require('./ticket');

mongoose
  .connect(config.MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log("MongoDB connection established.");
  })
  .catch(() => {
    console.error("There was an error while connecting to MongoDB");

    return process.exit(1);
  });

module.exports = { ExtractionModel, TicketModel };
