const models = require("../models");

async function getAll(req, res, next) {
  let ret = [];

  try {
    ret = await models.ExtractionModel.find({})
      .sort({ $natural: -1 });
  }
  catch (e) {
    console.error(e);
  }

  return res.json(ret);
}

async function save(req, res, next) {
  try {
    const newExtraction = new models.ExtractionModel({ ...req.body });

    await newExtraction.save();
    return res.json({ success: 1, description: 'Extraction stored in the database.' });
  }
  catch (e) {
    console.error(e);
    return res.status(500).json({ message: 'There was something wrong.' });
  }
}

module.exports = {
  getAll,
  save
}
