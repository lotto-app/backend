const models = require("../models");

async function getAll(req, res, next) {
  let ret = [];

  try {
    ret = await models.TicketModel.find({})
      .sort({ $natural: -1 });
  }
  catch (e) {
    console.error(e);
  }

  return res.json(ret);
}

async function save(req, res, next) {
  try {
    const newTicket = new models.TicketModel({ ...req.body });

    await newTicket.save();
    return res.json({ success: 1, description: 'Ticket stored in the database.' });
  }
  catch (e) {
    console.error(e);
    return res.status(500).json({ message: 'There was something wrong.' });
  }
}

async function getLastExtractionWinners(req, res, next) {
  try {
    const lastExtractions = await models.ExtractionModel.find({})
      .sort({ $natural: -1 })
      .limit(2);

    let ret;

    if (lastExtractions.length == 2) {
      const extractionDates = lastExtractions.map((extraction) => {
        return extraction.date;
      });

      const extractionHash = lastExtractions[1].numbersHash;

      ret = await models.TicketModel.find({
        numbersHash: extractionHash,
        date: {
          $gt: extractionDates[1],
          $lt: extractionDates[0]
        }
      });
    }
    else if (extractionDates.length > 0) {
      const extraction = lastExtractions[0];

      ret = await models.TicketModel.find({
        numbersHash: extraction.numbersHash,
        date: {
          $gt: extraction.date
        }
      });
    }
    else {
      ret = [];
    }

    return res.json(ret);
  }
  catch (e) {
    console.error(e);
    return res.status(500).json({ message: 'There was something wrong.' });
  }
}

async function getTicketWinners(req, res, next) {
  try {
    return res.json([]);
  }
  catch (e) {
    console.error(e);
    return res.status(500).json({ message: 'There was something wrong.' });
  }
}

module.exports = {
  getAll,
  save,
  getLastExtractionWinners,
  getTicketWinners
}
