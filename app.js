'use strict';

const express = require('express');
const app = express();

const SwaggerExpress = require('swagger-express-mw');
const swaggerUi = require('swagger-ui-express');
const yaml = require('yamljs');
const swaggerDocument = yaml.load('./api/swagger/swagger.yaml');

module.exports = app; // for testing

var config = {
  appRoot: __dirname // required config
};

SwaggerExpress.create(config, function (err, swaggerExpress) {
  if (err) { throw err; }

  // install middleware
  swaggerExpress.register(app);

  app.use('/', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

  var port = process.env.PORT || 8081;
  app.listen(port);

  console.log(`App started on port ${port}`);
});
