var should = require('should');
var request = require('supertest');
var server = require('../../../app');

describe('controllers', function () {
  describe('ticket', function () {
    describe('GET /ticket', function () {
      it('should return an array', function (done) {

        request(server)
          .get('/ticket')
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function (err, res) {
            should.not.exist(err);
            (Array.isArray(res.body)).should.be.true();

            done();
          });
      });
    });
  });

  describe('extraction', function () {
    describe('GET /extraction', function () {
      it('should return an array', function (done) {
        request(server)
          .get('/ticket')
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function (err, res) {
            should.not.exist(err);
            (Array.isArray(res.body)).should.be.true();

            done();
          });
      });
    });
  });
});
