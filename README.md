## Backend for the lotto application

### Usage:

#### Without Kubernetes:

```
$ yarn
$ yarn start
```

#### With Kubernetes:

If you're running inside a [Minikube](https://github.com/kubernetes/minikube) environment, make sure that you set you set your docker env appropriately:

```
$ eval $(minikube docker-env)
```

Then build your docker image:

```
$ docker build -t <username>/<name>:<tag> .
```

#### Example
Example:

```
$ docker build -t doidor/backend:v1 .
```

_Make sure to also update the k8s files if you use other username, name and tag._

Then apply the k8s config:

```
$ kubectl apply -f k8s/backend.yaml
```

There's also a [helm chart](https://helm.sh) that can be found in `k8s/backend`.
