# --- BASE ---
FROM node:10

WORKDIR /usr/src/backend

# Copy files to working directory
COPY yarn.lock .
COPY package.json .

# Install only dependencies for production
RUN yarn install

COPY . .

EXPOSE 10010

# Start the service
CMD [ "yarn", "start" ]

