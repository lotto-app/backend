require('dotenv');

let mongoUriTemp = "mongodb://localhost:27017/kubernetes-lotto";

if (
  process.env.MONGO_IP &&
  process.env.MONGO_USER &&
  process.env.MONGO_PASSWORD
) {
  mongoUriTemp = `mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_IP}:27017/kubernetes-lotto?authSource=admin`;
}

const MONGO_URI = mongoUriTemp;

module.exports = {
  MONGO_URI
}
